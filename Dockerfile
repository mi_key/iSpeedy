# Debian slim => python:3.11.2-slim (Debugging only)
# Alpine => python:3.11.2-alpine
FROM python:3.12.2-alpine

# Set the workingdirectory for the application
WORKDIR /app

# Add applications packages
RUN apk add --update --no-cache git build-base 

#Add the needded data and install the python app requirements
COPY requirements_collector.txt .
RUN pip install --no-cache-dir -r requirements_collector.txt

# Add files and directorys
RUN mkdir storage
COPY ./Software .

#Start the python application
CMD ["python", "-u", "iSpeedy_collector.py", "-m runtime"]