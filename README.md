# About

I encounter big issues with my network. From time to time my Internet speed drops down to a bare minimum. In order to know if the current state of my network is good or not i will use some gauges to show me the current max download and upload rate. 

In Addition I am not able to control the Router I am using for the Internet. Therefore I need to work something around the last project:

- https://www.instructables.com/Fritzmeter/

The general system design looks like this.


![alt text](Doc/SystemDesign-GeneralDesign.png)

We will collect the current max up and download speed with via an speed test for every 15 minutes.
The gathered information will than be send to an MQTT broker. An Raspberry pi zero will now take those Information and will display them on hardware gauges.
We need to do this Workaround since the raspberry pi zero will not be able to get all the available network speed via the speed test since all the available interfaces are slower...

In my setup i will read the current speed with an docker container on my Synology NAS. I added the Dockerfile as well.

![alt text](Doc/SystemDesign-MyImplementation.png)


# Hardware

- Raspberry Pi Zero W
- 2x Adafruit MCP4725 12-Bit DAC  https://learn.adafruit.com/mcp4725-12-bit-dac-tutorial
- 2x 5V Analog gauges

![Sketch](./Hardware/Sketch.png)

## Gauges Background

The Background of the gauges can be created by the Hardware/Analog Gauges/CreateGaugesBackground.py Python script.

There will be two svg files created. One for the up and one for the download. The script can be edited to suit the network settings.

![Sketch](./Hardware/Analog_Gauges/download.svg)

### Using the Python Gauge Background generator

#### Importing the needed libraries

To install pycairo we also need the dependencies of the C Headers. For mac os use:

```shell
brew install cairo pkg-config
```

For other os´s check this article: https://pycairo.readthedocs.io/en/latest/getting_started.html

After installing the requested c-headers we can install pycairo.

```shell
pip install pycairo
```
running the python script will create an output and show the file path of the two created files.

```shell
python3 CreateGaugesBackground.py \
Upload File saved here:/Users/esc/Git/FritzMeter_RaspberryPi/Hardware/upload.svg \
Download File saved here:/Users/esc/Git/FritzMeter_RaspberryPi/Hardware/download.svg \
Done
```

The output can be changed with the global constants ad the top

```ini
    UPLOAD_FILE_NAME = "upload.svg"
    DOWNLOAD_FILE_NAME = "download.svg"

    #Define the metrics of the gauge
    MAX_DOWNLOAD_SPEED = 200		#Set the max download speed here
    MAX_UPLOAD_SPEED = 30			#Set the max upload speed here

    BACKGROUND_SIZE_X = 650			#Fixed background settings. Do not change these.
    BACKGROUND_SIZE_Y = 400			#Fixed background settings. Do not change these.


    #Define the style of the gauges  
    FONT_LABEL = "Helvetica"			#Font of the labels
    FONT_LABEL_SIZE = 40				#Font size of the labels
    FONT_DESCRIPTION = "Helvetica"		#Font of the description
    FONT_DESCRIPTION_SIZE = 48			#Font size of the description


    UPLOAD_AXIS_DEVISION = 6			#Divisions of the upload gauges labels
    DOWNLOAD_AXIS_DEVISION = 4			#Divisions of the upload gauges labels
```

# Software

The application is written in python.


## adafruit-circuitpython-mcp4725

### Usage / Info 

- GitHub: https://github.com/adafruit/Adafruit_CircuitPython_MCP4725

- Library Definition: https://circuitpython.readthedocs.io/projects/mcp4725/en/latest/api.html
- Create an SPI object: https://circuitpython.readthedocs.io/en/latest/shared-bindings/busio/index.html#busio.I2C

## netifaces

### Usage / Info

- Library Definition: https://0xbharath.github.io/python-network-programming/libraries/netifaces/index.html

# Pi Konfiguration

After installing the latest Raspbian OS from RaspberryPi.org update the system by:

```shell
sudo apt-get update 
sudo apt-get upgrade
```
## Install the requested libraries

```shell
pip install fritzconnection adafruit-circuitpython-mcp4725 netifaces
```

## Create and start service

Source: https://www.nerdynat.com/programming/2019/run-python-on-your-raspberry-pi-as-background-service/

```shell
sudo nano /lib/systemd/system/iSpeedy.service
```

Add the text from ./Software/iSpeedy.service
Than change the permission


```shell
sudo chmod 644 /lib/systemd/system/iSpeedy.service
```

Reload the system manager configuration

```shell
sudo systemctl daemon-reload
```

Start the Service

```shell
sudo systemctl start iSpeedy.service
```

Enable the service to start at boot 

```shell
sudo systemctl enable iSpeedy.service
```

So stop the service you can use

```shell
sudo systemctl stop iSpeedy.service
```

# Background light

Additionally you can add an led backlight and control it with an button. In the script the button ist set to Pin 17 and the led's on pin 18 and 27.

![Sketch](./Hardware/BackgroundLed_Sketch_Schaltplan.png)

The requested _gpiozero_ lib should be installed with the pi os. If not you can add it with:

```shell
pip install gpiozero
```

## Add the service for the Background light control

```shell
sudo nano /lib/systemd/system/Background_Light.service
```

Add the text from ./Software/Background_Light.service
Than change the permission

```shell
sudo chmod 644 /lib/systemd/system/Background_Light.service
```

Reload the system manager configuration

```shell
sudo systemctl daemon-reload
```

Start the Service

```shell
sudo systemctl start Background_Light.service
```

Enable the service to start at boot 

```shell
sudo systemctl enable Background_Light.service
```