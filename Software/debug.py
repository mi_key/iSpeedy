import speedtest
from datetime import datetime

def main():

    try:
        st = speedtest.Speedtest()
        server = st.get_best_server()
        server_location = server.get("name")
        host = server.get("host")
        dl_speed = st.download() / 1000000
        ul_speed = st.upload() / 1000000
        print(f'SERVER: {host} | LOCATION: {server_location}\n')

        print(f'DOWNLOAD SPEED: {dl_speed}\n')
        print(f'UPLOAD SPEED: {ul_speed}\n')


    except Exception as inst:
        print(type(inst))    # the exception type
        print(inst.args)     # arguments stored in .args
        print(inst)  

#Call the function
main()
