import speedtest
import speedtest
import time
import paho.mqtt.client as mqtt
import os
import json
import time
from datetime import datetime
from zoneinfo import ZoneInfo
import traceback

TOPIC = "home/network/iSpeedTest"
CLIENT_ID =  "iSpeed_Collector"

class Collector:

    config:dict
    waitForConnect:bool
    networkTest:speedtest.Speedtest


    def __init__(self) -> None:
        """
        Init the collector class
        """

        self.waitForConnect = False
        self.networkTest = None


    def run(self):
        """
        Collecting the max down and upload values and send them to the mqtt broker
        """
        print("Start to collect the internet speed")

        blnLoopActive = True
        _valueUpload = 0
        _valueDownload = 0

        #Start the loop
        while blnLoopActive:

            try:

                # Read the Settings
                self.config = self.readSettings(path=os.path.join("storage", "config.json"))

                #get the speed
                _valueUpload, _valueDownload = self.getUpAndDownLoadSpeeds() 

                #send the data to te mqtt broker
                self.sendDataToMqttBroker(upload=_valueUpload, download=_valueDownload)

                # Log the Data to the console
                print(f"{datetime.now().astimezone(ZoneInfo("Europe/Zurich"))}      Upload MAX: {_valueUpload}      Download MAX: {_valueDownload}")

                #wait for 2s 
                time.sleep(self.config["speed_test"]["sleep"])

            except Exception as err:
                print(f"Error occurred. {str(err)}, /n {traceback.format_exc()}")
                time.sleep(120) 

    def readSettings(self, path:str)->dict:
        """
        Read the settings from the config file or environment variables

        Params
        ------
        path:str        path of the json config file

        Return
        ------
        dict            Settings as a dictionary
        """

        config = {}


        try:
            with open(path, 'r') as jsonFile:
                config = json.load(jsonFile)
        except Exception as err:
            print("warning could not read the settings file")


        return config

    def sendDataToMqttBroker(self, upload:any, download:any)->None:
        """
        Connect to the mqtt broker and send the Data

        Params
        ------
        upload:any              Upload value
        download:any            Download value

        Return
        ------
        None
        """

        client = self.createMqttClient()
        self.connectMqtt(client=client, config=self.config)
        publishUploadInfo = client.publish(TOPIC, json.dumps({"upload":upload, "download":download}))

    def getUpAndDownLoadSpeeds(self)->tuple[float, float]:
        """
        Get the up and download speed

        Params
        ------

        Return
        ------
        tuple[upload:float, download:float]         Will return a tuple with up and down load value
        """

        upLoad = 0
        downLoad = 0
        

        if self.networkTest == None:
            self.networkTest = speedtest.Speedtest()
            server = self.networkTest.get_best_server()

        try:
            #Convert to value to set the do dac 
            upLoad = self.convertToMbps(self.networkTest.upload()) 
            downLoad = self.convertToMbps(self.networkTest.download())
        except:
            self.networkTest = None
            raise Exception("Could not read the up and download speed")

        return (upLoad, downLoad)

    def convertToMbps(self, bytesPerSeconds):
        """
        Convert the Data (bytes per second) to Mbps Mega bits per second 

        Params
        ------
        bytesPerSeconds     Input bytes per second 

        Return
        ------ 
        float               return the calculated Mbps value
        """

        #1 Mega bit per Second (Mbps) = 1 000 000 Bits / Second
        #First convert to Bit (1 Byte = 8 Bit) Than convert to Mbps by divide with 1´000´000
        return float(bytesPerSeconds/1000000)

    def createMqttClient(self)->mqtt.Client:
        """
        Create the MQTT Client

        Return
        ------
        mqtt.Client     Returns teh created mqtt client
        """
        client = mqtt.Client(CLIENT_ID)
        client.on_connect = self.on_connect
        client.on_publish = self.on_publish
        client.will_set('{}/$announce'.format(TOPIC), payload='{}', retain=True)

        return client

    def connectMqtt(self, client:mqtt.Client, config:dict)-> None:
        """
        Connect to the mqtt broker

        Params
        ------
        client:mqtt.Client      paho mqtt client 
        config:dict             configuration dictionary

        Return
        bool                    will return true if connected = True // False otherwise
        """

        # Connect to the broker if not already connected
        client.connect(config["mqtt_broker"]["host"], config["mqtt_broker"]["port"])

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            print('MQTT connection established')
            self.waitForConnect = False
        else:
            print('Connection error with result code {} - {}'.format(str(rc), mqtt.connack_string(rc)), error=True)
            self.waitForConnect = False
            raise Exception("Connection to mqtt broker was not successful")

    def on_publish(self, client, userdata, mid):
        #print_line('Data successfully published.')
        pass


if __name__ == '__main__':

    #Start the main program
    print("Start the app")

    collectorApp = Collector()
    collectorApp.run()
