import os
import random
import json
from paho.mqtt import client as mqtt_client
import board
import busio
import adafruit_mcp4725
import threading
import time
import logging

TOPIC = "home/network/iSpeedTest"
MAX_UPLOAD_MBPS = 400        #chose the right maximum upload speed depending on your connection
MAX_DOWNLOAD_MBPS = 400     #chose the right maximum download speed depending on your connection

# Generate a Client ID with the subscribe prefix.
CLIENT_ID = "iSpeedy"

LOGGING_ACTIVE = False 

class iSpeedyDisplay:

    config:dict

    valueUploadOld:float
    valueDownloadOld:float

    def __init__(self) -> None:
        """
        Init the speedy class
        """
    
        # Set the logging definition
        logging.basicConfig(filename=os.path.join("var", "log", "history.log"), level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

        self.valueUploadOld = 0
        self.valueDownloadOld = 0

        # Get the settings
        self.config = self.readSettings(path=os.path.join("storage", "config.json"))
        i2c = busio.I2C(board.SCL, board.SDA)
        self.dacUpload = adafruit_mcp4725.MCP4725(i2c)
        self.dacDownload = adafruit_mcp4725.MCP4725(i2c, address=0x63)

    def readSettings(self, path:str)->dict:
        """
        Read the settings
        """

        config:dict
        with open(path, 'r') as jsonFile:
            config = json.load(jsonFile)
        
        return config

    def connect_mqtt(self) -> mqtt_client:

        client = mqtt_client.Client(CLIENT_ID)
        # client.username_pw_set(username, password)
        client.on_connect = self.on_connect
        client.connect(self.config["mqtt_broker"]["host"], self.config["mqtt_broker"]["port"])
        return client

    def on_connect(self, client, userdata, flags, rc):
        """
        On connect from the mqtt client
        """
        if rc == 0:
            print("Connected to MQTT Broker!")
            self.subscribe(client)
        else:
            print("Failed to connect, return code %d\n", rc)

    def subscribe(self, client: mqtt_client):
        """
        Subscribe to the mqtt tobics
        """
        client.subscribe(TOPIC)
        client.on_message = self.on_message

    def on_message(self, client, userdata, msg):
        """
        Messages received on the broker with the topic
        """
        print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")

        payloadAsJson = msg.payload.decode()
        payload = json.loads(payloadAsJson)
        _valueUpload = self.convertToAnalogOutput(MAX_UPLOAD_MBPS, payload.get("upload", "0"))
        uploadTransitionThread = threading.Thread(target=self.transition, args=(self.dacUpload, self.valueUploadOld, _valueUpload, 0.8, "Upload",))
        uploadTransitionThread.start()
        self.valueUploadOld = _valueUpload

        _valueDownload = self.convertToAnalogOutput(MAX_DOWNLOAD_MBPS, payload.get("download", "0"))
        downloadTransitionThread = threading.Thread(target=self.transition, args=(self.dacDownload, self.valueDownloadOld, _valueDownload, 0.8, "Download",))
        downloadTransitionThread.start()
        self.valueDownloadOld = _valueDownload

        if LOGGING_ACTIVE:
            _downloadMbs = payload.get("download", "0")
            _uploadMbs = payload.get("upload", "0")
            logging.info(f"Upload: {_uploadMbs} // Download: {_downloadMbs} ")


    def convertToAnalogOutput(self, maxMbps, currentMbps)->float:
        """
        Convert Mbps to Analog Output over I2C

        Params
        ------
        maxMbps         maximum Mbps
        currentMbps     current Mbps

        Return
        ------
        float           calculated value for the analog output
        """

        tmpRetVal = 65535/float(maxMbps)*float(currentMbps)

        return float(tmpRetVal)

    def convertToMbps(self, bytesPerSeconds):
        """
        BytesPerSeconds = Input 
        ---
        Convert the Data (bytes per second) to Mbps Mega bits per second 
        """

        #1 Mega bit per Second (Mbps) = 1 000 000 Bits / Second
        #First convert to Bit (1 Byte = 8 Bit) Than convert to Mbps by divide with 1´000´000
        return bytesPerSeconds/1000000

    def transition(self, dac, currentValue, newValue, timeSpan, name):
        """
        transition between the analog values
        Do this to prevent jumping pointer
        """

        intIterations = timeSpan/0.1
        blnCountUp = False
        lastValue = currentValue

        if (lastValue < newValue):
            blnCountUp = True

            valueDiff = (abs(newValue) - abs(lastValue)) / intIterations

        else:

            valueDiff = (abs(lastValue) - abs(newValue)) / intIterations

        for idx in range(int(intIterations)):

            if blnCountUp:
                currentValue = currentValue + valueDiff
            else:
                currentValue = currentValue - valueDiff
        
            #Set the Analog Value
            dac.value = int(currentValue)

            #wait for 100 ms 
            time.sleep(0.1)

    def run(self):

        loopActive = True
        while loopActive:
            
            try:
                client = self.connect_mqtt()
                client.loop_forever()
            except Exception as err:
                errStr = str(err)
                print(f"Exception occured: {errStr}")

if __name__ == '__main__':
    
    iSpeedyDisplayObject = iSpeedyDisplay() 
    iSpeedyDisplayObject.run()